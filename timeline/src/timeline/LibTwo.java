package timeline;

import timeline.TimeLine.RunningMsg;
import timeline.TimeLine.TLine;

public class LibTwo {
	@TLine(level = 1 ,copy = 15)
	public void method1(RunningMsg msg){
		System.out.println("level 1--B copy = "+msg.copy+" :"+Thread.currentThread());
	}
	
	@TLine(level = 1 ,copy = 6)
	public void method2(RunningMsg msg){
		System.out.println("level 1----A copy = "+msg.copy+" :"+Thread.currentThread());
	}
}

package timeline;

import timeline.TimeLine.Builder;

public class Bench {
	public static void main(String[] args) {
		new TimeLine.Builder()
		.addLib(new LibOne())
		.addLib(new LibTwo())
		.run();

	}

}
